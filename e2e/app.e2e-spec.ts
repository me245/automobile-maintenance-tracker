import { AutomobileMaintenanceTrackerPage } from './app.po';

describe('automobile-maintenance-tracker App', () => {
  let page: AutomobileMaintenanceTrackerPage;

  beforeEach(() => {
    page = new AutomobileMaintenanceTrackerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
