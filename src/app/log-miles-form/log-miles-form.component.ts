import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgIf } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-log-miles-form',
  templateUrl: './log-miles-form.component.html',
  styleUrls: ['./log-miles-form.component.css']
})
export class LogMilesFormComponent implements OnInit {
  @Input() automobileId: number;
  @Input() show: boolean;
  @Output() onMilesClose: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onMilesLogged: EventEmitter<number> = new EventEmitter<number>();

  milesForm: FormGroup;

  formErrors = {
    'miles': ''
  }

  validationMessages = {
    'miles': {
      'min': 'Cannot be less than zero.',
      'required': 'This Field is Required.'
    }
  }

  constructor(private fb: FormBuilder) {
   }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.milesForm = this.fb.group({
      miles: ['', [ Validators.required,
                  Validators.min(0) ] ]
    })
    this.milesForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.milesForm) { return; }
    const form = this.milesForm;
    for (const field of Object.keys(this.formErrors)) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  close() {
    this.onMilesClose.emit(!this.show);
    this.show = !this.show;
  }

  onSubmit() {
    this.onMilesLogged.emit(this.milesForm.value.miles as number);
    this.milesForm.reset();
    this.close();
  }

}
