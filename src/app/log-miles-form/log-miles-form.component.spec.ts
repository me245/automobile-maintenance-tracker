import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogMilesFormComponent } from './log-miles-form.component';
import { AppModule } from '../app.module';

describe('LogMilesFormComponent', () => {
  let component: LogMilesFormComponent;
  let fixture: ComponentFixture<LogMilesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogMilesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
