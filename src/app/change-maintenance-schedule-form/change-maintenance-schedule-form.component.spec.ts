import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMaintenanceScheduleFormComponent } from './change-maintenance-schedule-form.component';
import { AppModule } from '../app.module';
import { AutomobileManagerService } from '../automobile-manager.service';

const automobileManagerServiceStub = {
  bob: true,
  remove(id: number) {
    console.log(id);
  }
}

describe('ChangeMaintenanceScheduleFormComponent', () => {
  let component: ChangeMaintenanceScheduleFormComponent;
  let fixture: ComponentFixture<ChangeMaintenanceScheduleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [ {provide: AutomobileManagerService, useValue: automobileManagerServiceStub} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeMaintenanceScheduleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
