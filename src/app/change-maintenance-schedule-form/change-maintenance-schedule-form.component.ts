import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgIf } from '@angular/common';

import { AutomobileManagerService } from '../automobile-manager.service';

@Component({
  selector: 'app-change-maintenance-schedule-form',
  templateUrl: './change-maintenance-schedule-form.component.html',
  styleUrls: ['./change-maintenance-schedule-form.component.css']
})
export class ChangeMaintenanceScheduleFormComponent implements OnInit {
  @Input() automobileId: number;
  @Input() show: boolean;
  @Input() maintenanceAction: string;
  @Output() onMaintenanceClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  maintenanceForm: FormGroup;

  formErrors = {
    'milesBetweenEngineMaintenance': '',
    'milesBetweenTireRotation': '',
    'milesBetweenTireReplacement': '',
    'milesBetweenBrakeReplacement': ''
  }

  validationMessages = {
    'milesBetweenEngineMaintenance': {
      'min': 'Cannot be less than zero.',
      'required': 'This Field is Required.'
    },
    'milesBetweenTireRotation': {
      'min': 'Cannot be less than zero.',
      'required': 'This Field is Required.'
    },
    'milesBetweenTireReplacement': {
      'min': 'Cannot be less than zero.',
      'required': 'This Field is Required.'
    },
    'milesBetweenBrakeReplacement': {
      'min': 'Cannot be less than zero.',
      'required': 'This Field is Required.'
    }
  }

  constructor(private fb: FormBuilder, private automobileManagerService: AutomobileManagerService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.maintenanceForm = this.fb.group({
      milesBetweenEngineMaintenance: ['', [
                  Validators.required,
                  Validators.min(0) ] ],
      milesBetweenTireRotation: ['', [
                  Validators.required,
                  Validators.min(0) ] ],
      milesBetweenTireReplacement: ['', [
                  Validators.required,
                  Validators.min(0) ] ],
      milesBetweenBrakeReplacement: ['', [
                  Validators.required,
                  Validators.min(0) ] ]
    })
    this.maintenanceForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.maintenanceForm) { return; }
    const form = this.maintenanceForm;
    for (const field of Object.keys(this.formErrors)) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  close() {
    this.onMaintenanceClose.emit(!this.show);
    this.show = !this.show;
  }

  onSubmit() {
    this.automobileManagerService.updateSchedule(
      this.automobileId,
      this.maintenanceForm.value.milesBetweenEngineMaintenance,
      this.maintenanceForm.value.milesBetweenTireRotation,
      this.maintenanceForm.value.milesBetweenTireReplacement,
      this.maintenanceForm.value.milesBetweenBrakeReplacement,
    )
    this.maintenanceForm.reset();
    this.close();
  }

}
