import { Component, OnInit } from '@angular/core';
import { NgIf } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';

import { AutomobileManagerService } from '../automobile-manager.service';
import { AutomobileForm } from '../automobile';

@Component({
  selector: 'app-automobile-management',
  templateUrl: './automobile-management.component.html',
  styleUrls: ['./automobile-management.component.css']
})
export class AutomobileManagementComponent implements OnInit {
  automobiles: AutomobileForm[];
  message: string;
  private automobileSubscription: Subscription;
  private messageSubscription: Subscription;
  showNew: boolean;
  constructor(private automobileManagerService: AutomobileManagerService) { }

  ngOnInit() {
    this.showNew = false;
    this.getAutomobiles();
    this.getMessageSubscription();
    this.automobileManagerService.testMe();
  }

  getAutomobiles(): void {
    this.automobileSubscription = this.automobileManagerService.getAutomobiles().subscribe(
      autoList => this.automobiles = autoList
    );
  }

  getMessageSubscription(): void {
    this.messageSubscription = this.automobileManagerService.getMessage().subscribe( latest => {
      this.message = latest;
    })
  }

  deleteAutomobile(id: number): void {
    this.automobileManagerService.remove(id);
  }

  showNewModal() {
    this.showNew = true;
  }

  onNewClose(close: boolean) {
    this.showNew = close;
  }
}
