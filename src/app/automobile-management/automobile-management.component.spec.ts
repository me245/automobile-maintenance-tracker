import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AutomobileManagementComponent } from './automobile-management.component';
import { Automobile } from '../shared/automobile.model';
import { AutomobileManagerService } from '../automobile-manager.service';
import { AppModule } from '../app.module';

const automobileManagerServiceStub = {
  bob: true,
  remove(id: number) {
    console.log(id);
  },
  getAutomobiles() { return <BehaviorSubject<Automobile[]>>new BehaviorSubject([])},
  getMessage() { return <BehaviorSubject<string>>new BehaviorSubject('')},
  testMe() {
    console.log('hi there');
  }
}

describe('AutomobileManagementComponent', () => {
  let component: AutomobileManagementComponent;
  let fixture: ComponentFixture<AutomobileManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [ {provide: AutomobileManagerService, useValue: automobileManagerServiceStub} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomobileManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
