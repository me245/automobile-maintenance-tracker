import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { AutomobileManager } from './shared/automobile-manager.model';
import { AutomobileForm } from './automobile';

@Injectable()
export class AutomobileManagerService {
    private _idsInList: number[] = new Array<number>();
    private manager: AutomobileManager = new AutomobileManager();
    private _automobiles: BehaviorSubject<AutomobileForm[]>;
    private _message: BehaviorSubject<string>;
    upperIdNumberLimit = 100;
    constructor() {
        this._message = <BehaviorSubject<string>>new BehaviorSubject('');
        this._automobiles = <BehaviorSubject<AutomobileForm[]>>new BehaviorSubject([]);
        this._automobiles.next(this.autos);
    }

    private get autos(): AutomobileForm[] {
        return this.manager.automobiles.map(auto => {
            return new AutomobileForm(auto.automobileID,
                        auto.engineType(),
                        auto.make,
                        auto.model,
                        auto.engineMaintenanceAction(),
                        auto.year,
                        auto.odometer,
                        auto.milesUntilEngineMaintenance,
                        auto.milesUntilTireRotation,
                        auto.milesUntilTireReplacement,
                        auto.milesUntilBrakeReplacement,
                        auto.engineMaintenanceRequired(),
                        auto.tiresNeedToBeRotated(),
                        auto.tiresNeedToBeReplaced(),
                        auto.brakesNeedToBeReplaced());
        });
    }

    testMe() {
        this.add('Honda', 'Civic', 2011, 9532, 'gas');
        this.add('Tesla', 'T1', 2014, 9433, 'electric');
    }

    getAutomobiles(): Observable<AutomobileForm[]> {
        return this._automobiles.asObservable();
    }

    getMessage(): Observable<string> {
        return this._message.asObservable();
    }

    remove(id: number): void {
        if (this.manager.remove(id)) {
            this.pushMessage(`Removed Automobile ${id}`);
            this._automobiles.next(this.autos);
            this._idsInList.splice(this._idsInList.indexOf(id), 1);
        } else {
            this.pushMessage(`Failed to remove Automobile ${id}`);
        }
    }

    logMiles(id: number, amount: number) {
        try {
            this.manager.update(id).logMiles(amount);
            this._automobiles.next(this.autos);
        } catch (err) {
            this.pushMessage('Failed to log miles');
        }
    }

    maintainEngine(id: number): void {
        try {
            this.manager.update(id).maintainEngine();
            this._automobiles.next(this.autos);
        } catch (err) {
            this.pushMessage('Failed to maintain');
        }
    }

    rotateTires(id: number): void {
        try {
            this.manager.update(id).rotateTires();
            this._automobiles.next(this.autos);
        } catch (err) {
            this.pushMessage('Failed to Rotate Tires');
        }
    }

    replaceTires(id: number): void {
        try {
            this.manager.update(id).replaceTires();
            this._automobiles.next(this.autos);
        } catch (err) {
            this.pushMessage('Failed to Replace Tires');
        }
    }

    replaceBrakes(id: number): void {
        try {
            this.manager.update(id).replaceBrakes();
            this._automobiles.next(this.autos);
        } catch (err) {
            this.pushMessage('Failed to Replace Brakes');
        }
    }

    updateSchedule(id: number,
        milesBetweenEngineMaintenace: number,
        milesBetweenTireRotation: number,
        milesBetweenTireReplacement: number,
        milesBetweenBrakeReplacement: number) {
        try {
            this.manager.updateSchedule(id,
                milesBetweenEngineMaintenace,
                milesBetweenTireRotation,
                milesBetweenTireReplacement,
                milesBetweenBrakeReplacement)
            this._automobiles.next(this.autos);
        } catch (err) {
            console.log(err);
            this.pushMessage('Failed to Update Schedule');
        }
    }

    add(make: string, model: string, year: number, odometer: number, type: string, id?: number): void {
        if (id && this._idsInList.indexOf(id) !== -1) {
            this.pushMessage(`Failed to re add Automobile with ID: ${id}`);
        } else if (!id) {
            id = Math.floor(Math.random() * this.upperIdNumberLimit) + 1;
            while (this._idsInList.indexOf(id) !== -1) {
                id = Math.floor(Math.random() * this.upperIdNumberLimit) + 1;
            }
        }
        if (this.manager.add(id, make, model, year, odometer, type)) {
            this.pushMessage('Added a new Automobile');
            this._automobiles.next(this.autos);
            this._idsInList.push(id);
        } else {
            this.pushMessage(`Failed to add a new Automobile of type: ${type}`);
        }
    }

    private pushMessage(message: string): void {
        this._message.next(message);
        setTimeout(() => {
            this._message.next('');
        }, 5000);
    }
}
