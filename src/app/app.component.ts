import { Component } from '@angular/core';

import { AutomobileManagerService } from './automobile-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AutomobileManagerService]
})
export class AppComponent {
  title = 'Automobile Maintenance Tracker';
  constructor(private automobileManagerService: AutomobileManagerService) { }
}
