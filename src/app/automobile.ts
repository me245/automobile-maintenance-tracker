// This is a form abstraction for Automobile class

export class AutomobileForm {
    constructor(
        public id: number,
        public type: string,
        public make: string,
        public model: string,
        public engineMaintenanceAction: string,
        public year: number,
        public odometer: number,
        public milesUntilEngineMaintenance: number,
        public milesUntilTireRotation: number,
        public milesUntilTireReplacement: number,
        public milesUntilBrakeReplacement: number,
        public needsEngineMaintenance: boolean,
        public needsTireRotation: boolean,
        public needsTireReplacement: boolean,
        public needsBrakeReplacement: boolean
    ) { }
}

