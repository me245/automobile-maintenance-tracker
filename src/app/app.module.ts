import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AutomobileComponent } from './automobile/automobile.component';
import { AutomobileManagementComponent } from './automobile-management/automobile-management.component';
import { LogMilesFormComponent } from './log-miles-form/log-miles-form.component';
import { ChangeMaintenanceScheduleFormComponent } from './change-maintenance-schedule-form/change-maintenance-schedule-form.component';
import { AddNewVehicleFormComponent } from './add-new-vehicle-form/add-new-vehicle-form.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AutomobileComponent,
    AutomobileManagementComponent,
    LogMilesFormComponent,
    ChangeMaintenanceScheduleFormComponent,
    AddNewVehicleFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
