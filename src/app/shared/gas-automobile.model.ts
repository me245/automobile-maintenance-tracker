import { Automobile } from './automobile.model';
import { Engine } from './engine.model';
import { EngineType } from './engine-type.enum';
import { MaintenanceSchedule } from './maintenance-schedule.model';
import { AutomobileDescription } from './automobile-description.model';

export class GasAutomobile extends Automobile {
    constructor(
        uniqueID: number,
        odometer: number,
        automobileDescription: AutomobileDescription,
        maintenanceSchedule: MaintenanceSchedule = new MaintenanceSchedule()
    ) {
        super(uniqueID, odometer, new Engine(EngineType.Gas), automobileDescription, maintenanceSchedule)
    }
    engineMaintenanceAction(): string {
        return 'Change Oil';
    }
}
