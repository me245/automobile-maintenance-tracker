export interface IMaintainable {
    engineMaintenanceRequired(): boolean;
    brakesNeedToBeReplaced(): boolean;
    tiresNeedToBeReplaced(): boolean;
    tiresNeedToBeRotated(): boolean;
    maintainEngine();
    replaceTires();
    rotateTires();
    replaceBrakes();
}
