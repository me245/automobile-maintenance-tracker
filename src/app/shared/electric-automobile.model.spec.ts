import { ElectricAutomobile } from './electric-automobile.model';
import { Engine } from './engine.model';
import { EngineType } from './engine-type.enum';
import { MaintenanceSchedule } from './maintenance-schedule.model';
import { AutomobileDescription } from './automobile-description.model';

describe('In the file electric-automobile.model.ts', () => {
    describe('the ElectricAutombile\'s', () => {
        const automobileDescription: AutomobileDescription = new AutomobileDescription('Tesla', 'T1', 2014);
        let sut: ElectricAutomobile;
        beforeEach(() => {
            sut = new ElectricAutomobile(37, 9532, automobileDescription);
        })
        describe('constructor', () => {
            it('should be electric', () => {
                expect(sut.engineType()).toEqual(EngineType[EngineType.Electric]);
            })
            it('should allow custom schedule', () => {
                    const customSched: MaintenanceSchedule = new MaintenanceSchedule(4000, 50000, 5000, 50000);
                    sut = new ElectricAutomobile(37, 9532, automobileDescription, customSched);
                    expect(sut.maintenanceSchedule).toBe(customSched);
            })
            it('Should not allow negative odometer entry', () => {
                expect(() => {
                    sut = new ElectricAutomobile(37, -9532, automobileDescription);
                }).toThrowError('odometer cannot be negative');
            })
            it('should set make to Tesla', () => {
                expect(sut.make).toEqual('Tesla');
            })
            it('should set model to T1', () => {
                expect(sut.model).toEqual('T1');
            })
            it('should set year to 2014', () => {
                expect(sut.year).toEqual(2014);
            })
            it('should set sechdule to default schedule', () => {
                expect(sut.maintenanceSchedule).toEqual(new MaintenanceSchedule(100000, 50000, 6000, 50000));
            })
        })
        describe('engineMaintenanceAction', () => {
            it('should return Change Battery', () => {
                expect(sut.engineMaintenanceAction()).toEqual('Change Battery');
            })
        })
        describe('engineMaintenanceRequired', () => {
            it('should return false if odometer hasn\'t changed since last engine maintenance', () => {
                expect(sut.engineMaintenanceRequired()).toBeFalsy();
            })
            it('should return false if odometer is less than 100000 miles since last engine maintenance', () => {
                sut.logMiles(99999);
                expect(sut.engineMaintenanceRequired()).toBeFalsy();
            })
            it('should return true if odometer is equal to 100000 miles since last engine maintenance', () => {
                sut.logMiles(100000);
                expect(sut.engineMaintenanceRequired()).toBeTruthy();
            })
            it('should return true if odometer is more than 100000 miles since last engine maintenance', () => {
                sut.logMiles(100001);
                expect(sut.engineMaintenanceRequired()).toBeTruthy();
            })
        })
        describe('brakesNeedToBeReplaced', () => {
            it('should return false if odometer hasn\'t changed since last brake replacement', () => {
                expect(sut.brakesNeedToBeReplaced()).toBeFalsy();
            })
            it('should return false if odometer is less than 50000 miles since last brake replacement', () => {
                sut.logMiles(2999);
                expect(sut.brakesNeedToBeReplaced()).toBeFalsy();
            })
            it('should return true if odometer is equal to 50000 miles since last brake replacement', () => {
                sut.logMiles(50000);
                expect(sut.brakesNeedToBeReplaced()).toBeTruthy();
            })
            it('should return true if odometer is more than 50000 miles since last brake replacement', () => {
                sut.logMiles(50001);
                expect(sut.brakesNeedToBeReplaced()).toBeTruthy();
            })
        })
        describe('tiresNeedToBeReplaced', () => {
            it('should return false if odometer hasn\'t changed since last tire replacement', () => {
                expect(sut.tiresNeedToBeReplaced()).toBeFalsy();
            })
            it('should return false if odometer is less than 50000 miles since last tire replacement', () => {
                sut.logMiles(2999);
                expect(sut.tiresNeedToBeReplaced()).toBeFalsy();
            })
            it('should return true if odometer is equal to 50000 miles since last tire replacement', () => {
                sut.logMiles(50000);
                expect(sut.tiresNeedToBeReplaced()).toBeTruthy();
            })
            it('should return true if odometer is more than 50000 miles since last tire replacement', () => {
                sut.logMiles(50001);
                expect(sut.tiresNeedToBeReplaced()).toBeTruthy();
            })
        })
        describe('tiresNeedToBeRotated', () => {
            it('should return false if odometer hasn\'t changed since last tire rotation', () => {
                expect(sut.tiresNeedToBeRotated()).toBeFalsy();
            })
            it('should return false if odometer is less than 6000 miles since last tire rotation', () => {
                sut.logMiles(2999);
                expect(sut.tiresNeedToBeRotated()).toBeFalsy();
            })
            it('should return true if odometer is equal to 6000 miles since last tire rotation', () => {
                sut.logMiles(6000);
                expect(sut.tiresNeedToBeRotated()).toBeTruthy();
            })
            it('should return true if odometer is more than 6000 miles since last tire rotation', () => {
                sut.logMiles(6001);
                expect(sut.tiresNeedToBeRotated()).toBeTruthy();
            })
        })
        describe('logMiles', () => {
            it('should set odometer forward by 2553 miles', () => {
                sut.logMiles(2553);
                expect(sut.odometer).toEqual(9532 + 2553);
            })
            it('should throw an error if a negative number is used', () => {
                expect(() => sut.logMiles(-1)).toThrowError('odometer can\'t be moved backwards');
            })
        })
        describe('maintainEngine', () => {
            it('should set a true response on engineRequiresMaintenance to false', () => {
                sut.logMiles(3000);
                sut.maintainEngine();
                expect(sut.engineMaintenanceRequired()).toBeFalsy();
            })
        })
        describe('replaceTires', () => {
            it('should set a true response on tiresNeedToBeReplaced to false', () => {
                sut.logMiles(50000);
                sut.maintainEngine();
                expect(sut.engineMaintenanceRequired()).toBeFalsy();
            })
        })
        describe('rotateTires', () => {
            it('should set a true response on tiresNeedToBeRotated to false', () => {
                sut.logMiles(6000);
                sut.maintainEngine();
                expect(sut.engineMaintenanceRequired()).toBeFalsy();
            })
        })
        describe('replaceBrakes', () => {
            it('should set a true response on brakesNeedToBeReplaced to false', () => {
                sut.logMiles(50000);
                sut.maintainEngine();
                expect(sut.engineMaintenanceRequired()).toBeFalsy();
            })
        })
    })
})
