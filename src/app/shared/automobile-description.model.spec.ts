import { AutomobileDescription } from './automobile-description.model';

describe('In the file automobile-description.model.ts', () => {
    describe('the AutomobileDescription\'s constructor', () => {
        let sut: AutomobileDescription;
        beforeEach(() => {
            sut = new AutomobileDescription('Buick', 'LeSabre', 1997);
        })
        it('should set make to Buick', () => {
            expect(sut.make).toEqual('Buick');
        })
        it('should set model to LeSabre', () => {
            expect(sut.model).toEqual('LeSabre');
        })
        it('should set year to 1997', () => {
            expect(sut.year).toEqual(1997);
        })
    })
})
