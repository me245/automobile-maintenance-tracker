export class MaintenanceSchedule {
    constructor(
        readonly milesBetweenEngineMaintenance: number = 3000,
        readonly milesBetweenTireChange: number = 50000,
        readonly milesBetweenTireRotation: number = 6000,
        readonly milesBetweenBrakeReplacement: number = 50000,
    ) {
        if (milesBetweenEngineMaintenance < 0) {
            throw new RangeError('Engine Maintenance Miles must not be negative');
        }
        if (milesBetweenTireChange < 0) {
            throw new RangeError('Tire Change Miles must not be negative');
        }
        if (milesBetweenTireRotation < 0) {
            throw new RangeError('Tire Rotation Miles must not be negative');
        }
        if (milesBetweenBrakeReplacement < 0) {
            throw new RangeError('Brake Replacement Miles must not be negative');
        }
    }
}
