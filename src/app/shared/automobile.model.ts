import { Engine } from './engine.model';
import { EngineType } from './engine-type.enum';
import { MaintenanceSchedule } from './maintenance-schedule.model';
import { AutomobileDescription } from './automobile-description.model';
import { IMaintainable } from './maintainable.interface';

export abstract class Automobile implements IMaintainable {
    protected _milesWhenEngineMaintained: number;
    protected _milesWhenTiresReplaced: number;
    protected _milesWhenTiresRotated: number;
    protected _milesWhenBrakesReplaced: number;

    constructor(
        readonly automobileID: number,
        protected _odometer: number,
        protected _engine: Engine,
        protected _automobileDescription: AutomobileDescription,
        public maintenanceSchedule: MaintenanceSchedule
    ) {
        if (_odometer < 0) {
            throw new RangeError('odometer cannot be negative');
        }
        this._milesWhenEngineMaintained = _odometer;
        this._milesWhenTiresReplaced = _odometer;
        this._milesWhenTiresRotated = _odometer;
        this._milesWhenBrakesReplaced = _odometer;
    }

    engineMaintenanceRequired(): boolean {
        return (this._odometer - this._milesWhenEngineMaintained) >= this.maintenanceSchedule.milesBetweenEngineMaintenance;
    }

    brakesNeedToBeReplaced(): boolean {
        return (this._odometer - this._milesWhenBrakesReplaced) >= this.maintenanceSchedule.milesBetweenBrakeReplacement;
    }

    tiresNeedToBeReplaced(): boolean {
        return (this._odometer - this._milesWhenTiresReplaced) >= this.maintenanceSchedule.milesBetweenTireChange;
    }

    tiresNeedToBeRotated(): boolean {
        return (this._odometer - this._milesWhenTiresRotated) >= this.maintenanceSchedule.milesBetweenTireRotation;
    }

    engineType(): string {
        return EngineType[this._engine.Type];
    }

    get odometer(): number {
        return this._odometer;
    }

    get make(): string {
        return this._automobileDescription.make;
    }
    get model(): string {
        return this._automobileDescription.model;
    }
    get year(): number {
        return this._automobileDescription.year;
    }

    get milesUntilEngineMaintenance(): number {
        return this.engineMaintenanceRequired() ? 0 :
            this.maintenanceSchedule.milesBetweenEngineMaintenance - (this._odometer - this._milesWhenEngineMaintained);
    }
    get milesUntilTireRotation(): number {
        return this.tiresNeedToBeRotated() ? 0 :
            this.maintenanceSchedule.milesBetweenTireRotation - (this._odometer - this._milesWhenTiresRotated);
    }
    get milesUntilTireReplacement(): number {
        return this.tiresNeedToBeReplaced() ? 0 :
            this.maintenanceSchedule.milesBetweenTireChange - (this._odometer - this._milesWhenTiresReplaced);
    }
    get milesUntilBrakeReplacement(): number {
        return this.brakesNeedToBeReplaced() ? 0 :
            this.maintenanceSchedule.milesBetweenBrakeReplacement - (this._odometer - this._milesWhenBrakesReplaced);
    }

    abstract engineMaintenanceAction(): string;

    logMiles(milesDriven: number) {
        if (milesDriven < 0) {
            throw new RangeError('odometer can\'t be moved backwards');
        }
        this._odometer += milesDriven;
    }

    maintainEngine() {
        this._milesWhenEngineMaintained = this._odometer;
    }

    replaceTires() {
        this._milesWhenTiresReplaced = this._odometer;
    }

    rotateTires() {
        this._milesWhenTiresRotated = this._odometer;
    }

    replaceBrakes() {
        this._milesWhenBrakesReplaced = this._odometer;
    }
}
