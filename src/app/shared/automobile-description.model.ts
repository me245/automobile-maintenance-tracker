export class AutomobileDescription {
    constructor(
        readonly make: string,
        readonly model: string,
        readonly year: number
    ) {}
}
