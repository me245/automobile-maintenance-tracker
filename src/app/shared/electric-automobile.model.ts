import { Automobile } from './automobile.model';
import { Engine } from './engine.model';
import { EngineType } from './engine-type.enum';
import { MaintenanceSchedule } from './maintenance-schedule.model';
import { AutomobileDescription } from './automobile-description.model';

export class ElectricAutomobile extends Automobile {
    constructor(
        uniqueID: number,
        odometer: number,
        automobileDescription: AutomobileDescription,
        maintenanceSchedule: MaintenanceSchedule = new MaintenanceSchedule(100000, 50000, 6000, 50000)
    ) {
        super(uniqueID, odometer, new Engine(EngineType.Electric), automobileDescription, maintenanceSchedule)
    }
    engineMaintenanceAction(): string {
        return 'Change Battery';
    }
}
