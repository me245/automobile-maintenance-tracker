import { MaintenanceSchedule } from './maintenance-schedule.model';

describe('In the file maintenance-schedule.model.ts', () => {
    describe('the MaintenanceSchedule\'s ', () => {
        let sut: MaintenanceSchedule;
        describe('default constructor', () => {
            beforeEach(() => {
                sut = new MaintenanceSchedule();
            })
            it('should set the Miles for Engine to 3000', () => {
                expect(sut.milesBetweenEngineMaintenance).toEqual(3000);
            })
            it('should set the Miles for Tire Change to 50000', () => {
                expect(sut.milesBetweenTireChange).toEqual(50000);
            })
            it('should set the Miles for Tire Rotation to 6000', () => {
                expect(sut.milesBetweenTireRotation).toEqual(6000);
            })
            it('should set the Miles for Brake Replacement to 50000', () => {
                expect(sut.milesBetweenBrakeReplacement).toEqual(50000);
            })
        })
        describe('constructor', () => {
            describe('should', () => {
                beforeEach(() => {
                    sut =  new MaintenanceSchedule(100000, 40000, 5000, 60000);
                })
                it('allow Miles for Engine to be set to 100000', () => {
                    expect(sut.milesBetweenEngineMaintenance).toEqual(100000);
                })
                it('allow Miles for Tire Change to be set to 40000', () => {
                    expect(sut.milesBetweenTireChange).toEqual(40000);
                })
                it('allow Miles for Tire Rotation to be set to 5000', () => {
                    expect(sut.milesBetweenTireRotation).toEqual(5000);
                })
                it('allow Miles for Brake Replacement to be set to 60000', () => {
                    expect(sut.milesBetweenBrakeReplacement).toEqual(60000);
                })
            })
            describe('should not', () => {
                it('allow negative numbers for Engine Miles', () => {
                    expect(() => sut = new MaintenanceSchedule(-1)).toThrowError('Engine Maintenance Miles must not be negative');
                })
                it('allow negative numbers for Tire Change Miles', () => {
                    expect(() => sut = new MaintenanceSchedule(0, -1)).toThrowError('Tire Change Miles must not be negative');
                })
                it('allow negative numbers for Tire Rotation Miles', () => {
                    expect(() => sut = new MaintenanceSchedule(0, 0, -1)).toThrowError('Tire Rotation Miles must not be negative');
                })
                it('allow negative numbers for Brake Replacement Miles', () => {
                    expect(() => sut = new MaintenanceSchedule(0, 0, 0, -1)).toThrowError('Brake Replacement Miles must not be negative');
                })
            })
        })
    })
})
