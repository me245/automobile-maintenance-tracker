import { Automobile } from './automobile.model';
import { GasAutomobile } from './gas-automobile.model';
import { ElectricAutomobile } from './electric-automobile.model';
import { DieselAutomobile } from './diesel-automobile.model';
import { AutomobileDescription } from './automobile-description.model';
import { MaintenanceSchedule } from './maintenance-schedule.model';

export class AutomobileManager {
    private _idsInList: number[] = new Array<number>();

    constructor(
        private _automobiles:  Automobile[] = new Array<Automobile>()
    ) {}

    get automobiles(): Automobile[] {
        const autombileList: Automobile[] = new Array<Automobile>();
        this._automobiles.forEach((auto) => {
            autombileList.push(auto);
        })
        return autombileList;
    }

    add(id: number, make: string, model: string, year: number, odometer: number, type: string): boolean {
        let automobile: Automobile;
        const description = new AutomobileDescription(make, model, year);
        switch (type.toLowerCase()) {
            case 'gas':
                automobile = new GasAutomobile(id, odometer, description);
                break;
            case 'diesel':
                automobile = new DieselAutomobile(id, odometer, description);
                break;
            case 'electric':
                automobile = new ElectricAutomobile(id, odometer, description);
                break;
            default:
                return false;
        }
        return this.postNewVehicle(automobile);
    }

    remove (uniqueID: number): boolean {
        const index: number = this._automobiles.findIndex( (auto) => {
            return auto.automobileID === uniqueID;
        });
        if (index !== -1) {
            this._automobiles.splice(index, 1);
            return true;
        } else {
            return false;
        }
    }

    update (uniqueID: number): Automobile {
        const index: number = this._automobiles.findIndex( (auto) => {
            return auto.automobileID === uniqueID;
        });
        if (index !== -1) {
            return this._automobiles[index];
        } else {
            return null;
        }
    }

    updateSchedule(id: number,
        milesBetweenEngineMaintenace: number,
        milesBetweenTireRotation: number,
        milesBetweenTireReplacement: number,
        milesBetweenBrakeReplacement: number): boolean {
        const schedule = new MaintenanceSchedule(milesBetweenEngineMaintenace,
            milesBetweenTireReplacement,
            milesBetweenTireRotation,
            milesBetweenBrakeReplacement);
        return this.postSchedule(id, schedule);
    }

    private postSchedule(id: number, schedule: MaintenanceSchedule): boolean {
        try {
            this.update(id).maintenanceSchedule = schedule;
            return true;
        } catch (err) {
            return false;
        }
    }

    private postNewVehicle (automobile: Automobile): boolean {
        if ( this._automobiles.findIndex( (auto) => {
            return auto.automobileID === automobile.automobileID;
        }) === -1) {
            this._automobiles.push(automobile);
            return true;
        } else {
            return false;
        }
    }

}
