import { EngineType } from './engine-type.enum';

export class Engine {
    constructor( public Type: EngineType ) { }
}
