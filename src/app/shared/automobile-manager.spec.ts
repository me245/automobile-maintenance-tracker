import { AutomobileManager } from './automobile-manager.model';
import { GasAutomobile } from './gas-automobile.model';
import { ElectricAutomobile } from './electric-automobile.model';
import { DieselAutomobile } from './diesel-automobile.model';
import { Automobile } from './automobile.model';
import { Engine } from './engine.model';
import { EngineType } from './engine-type.enum';
import { MaintenanceSchedule } from './maintenance-schedule.model';
import { AutomobileDescription } from './automobile-description.model';

describe('In the file automobile-manager.model.ts', () => {
    describe('the AutomobileManager\'s ', () => {
        const electric: ElectricAutomobile = new ElectricAutomobile(31, 9532, new AutomobileDescription(
            'Tesla',
            'T1',
            2014
        ));
        const gas: GasAutomobile = new GasAutomobile(32, 9533, new AutomobileDescription(
            'Honda',
            'Civic',
            2007
        ));
        const diesel: DieselAutomobile = new DieselAutomobile(33, 9544, new AutomobileDescription(
            'Dodge',
            'Ram',
            2011
        ));
        let sut: AutomobileManager;
        beforeEach(() => {
            sut = new AutomobileManager();
        })
        describe('constructor', () => {
            it('should have an empty list on default', () => {
                expect(sut.automobiles.length).toEqual(0);
            })
            it('should accept a list containing a Tesla, Honda, and a Dodge', () => {
                sut = new AutomobileManager([electric, gas, diesel]);
                expect(sut.automobiles.length).toEqual(3);
            })
        })
        describe('add', () => {
            it('should increase automobile count by 1 when adding electric', () => {
                const count = sut.automobiles.length;
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                expect(sut.automobiles.length).toEqual(count + 1);
            })
            it('should add electric to the automobiles when electric is not in the list', () => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                expect(sut.automobiles).toContain(electric);
            })
            it('should return true if it adds electric when electric is not there', () => {
                expect(sut.add(
                    electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType()))
                    .toBeTruthy();
            })
            it('should return false when adding electric when electric already present', () => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                expect(sut.add(
                    electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType()))
                    .toBeFalsy();
            })
            it('should not increase automobile count by 1 when trying to add electric a second time', () => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                const count = sut.automobiles.length;
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                expect(sut.automobiles.length).toEqual(count);
            })
            it('should only have one electric when adding electric a second time', () => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
                expect(sut.automobiles.filter((auto) => auto.automobileID === electric.automobileID).length).toEqual(1);
            })
        })
        describe('remove', () => {
            beforeEach(() => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
            })
            it('should decrease the count of automobiles by 1 when removing electric', () => {
                const count = sut.automobiles.length;
                sut.remove(electric.automobileID);
                expect(sut.automobiles.length).toEqual(count - 1);
            })
            it('should no longer have electric after removing electric', () => {
                sut.remove(electric.automobileID);
                expect(sut.automobiles).not.toContain(electric);
            })
            it('should return true when removing electric', () => {
                expect(sut.remove(electric.automobileID)).toBeTruthy();
            })
            it('should return false when removing diesel', () => {
                expect(sut.remove(diesel.automobileID)).toBeFalsy();
            })
            it('should not change the number of automobiles when removing diesel', () => {
                const count = sut.automobiles.length;
                sut.remove(diesel.automobileID);
                expect(sut.automobiles.length).toEqual(count);
            })
        })
        describe('update', () => {
            beforeEach(() => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
            })
            it('should return electric when asking for electric', () => {
                expect(sut.update(electric.automobileID)).toEqual(electric);
            })
            it('should return null when asking for diesel', () => {
                expect(sut.update(diesel.automobileID)).toBeNull();
            })
        })
        describe('updateSchedule', () => {
            beforeEach(() => {
                sut.add(electric.automobileID, electric.make, electric.model, electric.year, electric.odometer, electric.engineType());
            })
            it('should return true when updating electric\'s schedule', () => {
                expect(sut.updateSchedule(electric.automobileID, 4000, 5000, 50000, 50000)).toBeTruthy();
            })
            it('should return false when updating diesel\'s schedule when not added', () => {
                expect(sut.updateSchedule(diesel.automobileID, 4000, 5000, 50000, 50000)).toBeFalsy();
            })
            it('should throw an error when updating electric\'s schedule with a negative value', () => {
                expect(() => sut.updateSchedule(electric.automobileID, -4000, 5000, 50000, 50000))
                    .toThrowError('Engine Maintenance Miles must not be negative');
            })
            it('should update the schedule when updating electric\'s schedule', () => {
                sut.updateSchedule(electric.automobileID, 4000, 5000, 50000, 50000)
                expect(sut.automobiles[0].maintenanceSchedule.milesBetweenEngineMaintenance).toEqual(4000);
            })
        })
    })
})
