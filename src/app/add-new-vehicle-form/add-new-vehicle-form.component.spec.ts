import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewVehicleFormComponent } from './add-new-vehicle-form.component';
import { AppModule } from '../app.module';
import { AutomobileManagerService } from '../automobile-manager.service';


const automobileManagerServiceStub = {
  bob: true,
  remove(id: number) {
    console.log(id);
  }
}

describe('AddNewVehicleFormComponent', () => {
  let component: AddNewVehicleFormComponent;
  let fixture: ComponentFixture<AddNewVehicleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [ {provide: AutomobileManagerService, useValue: automobileManagerServiceStub} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewVehicleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
