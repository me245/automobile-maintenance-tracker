import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgIf } from '@angular/common';

import { AutomobileManagerService } from '../automobile-manager.service';

@Component({
  selector: 'app-add-new-vehicle-form',
  templateUrl: './add-new-vehicle-form.component.html',
  styleUrls: ['./add-new-vehicle-form.component.css']
})
export class AddNewVehicleFormComponent implements OnInit {
  @Input() show: boolean;
  @Output() onNewVehicleClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  newVehicleForm: FormGroup;

  formErrors = {
    'make': '',
    'model': '',
    'year': '',
    'odometer': '',
    'id': ''
  }

  validationMessages = {
    'make': {
      'required': 'Make is Required.'
    },
    'model': {
      'required': 'Model is Required.'
    },
    'year': {
      'min': 'Cannot be less than 1900.',
      'max': 'Cannot be more than 2018.',
      'required': 'Year is Required.'
    },
    'odometer': {
      'min': 'Cannot be less than zero.',
      'required': 'Odometer is Required.'
    },
    'id': {
      'min': 'ID Cannot be less than zero'
    }
  }

  constructor(private fb: FormBuilder, private automobileManagerService: AutomobileManagerService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.newVehicleForm = this.fb.group({
      make: ['', [
                  Validators.required,
                  Validators.min(0) ] ],
      model: ['', [
                  Validators.required,
                  Validators.min(0) ] ],
      year: ['', [
                  Validators.required,
                  Validators.min(1900),
                  Validators.max(2018) ] ],
      odometer: ['', [
                  Validators.required,
                  Validators.min(0) ] ],
      type: '',
      id: ['',  Validators.min(0) ]
    })
    this.newVehicleForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.newVehicleForm) { return; }
    const form = this.newVehicleForm;
    for (const field of Object.keys(this.formErrors)) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key of Object.keys(control.errors)) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  close() {
    this.onNewVehicleClose.emit(!this.show);
    this.show = !this.show;
  }

  onSubmit() {
    this.automobileManagerService.add(
      this.newVehicleForm.value.make,
      this.newVehicleForm.value.model,
      this.newVehicleForm.value.year,
      this.newVehicleForm.value.odometer,
      this.newVehicleForm.value.type,
      this.newVehicleForm.value.id
    );
    this.newVehicleForm.reset();
    this.close();
  }

}
