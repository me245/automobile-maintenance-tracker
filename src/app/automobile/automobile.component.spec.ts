import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { AutomobileComponent } from './automobile.component';
import { AutomobileManagerService } from '../automobile-manager.service';
import { AutomobileForm } from '../automobile';
import { AppModule } from '../app.module';


const automobileManagerServiceStub = {
  bob: true,
  logMiles(id: number, amount: number) {
    console.log(id);
  },
  maintainEngine(id: number) {
    console.log(id);
  },
  rotateTires(id: number) {
    console.log(id);
  },
  replaceTires(id: number) {
    console.log(id);
  },
  replaceBrakes(id: number) {
    console.log(id);
  }
}

describe('AutomobileComponent', () => {
  let component: AutomobileComponent;
  let fixture: ComponentFixture<AutomobileComponent>;
  let expectedAuto: AutomobileForm;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [ {provide: AutomobileManagerService, useValue: automobileManagerServiceStub} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomobileComponent);
    component = fixture.componentInstance;
    expectedAuto = new AutomobileForm(33, 'Gas','Honda','Civic','Change Oil', 2011, 9532, 3000, 1, 1, 1, false, false, false, false);
    component.automobile = expectedAuto;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have an automobile ID of 33 in a header div', () => {
    const debugElement = fixture.debugElement;
    expect(debugElement.nativeElement.querySelector('.header').textContent).toContain('Automobile 33');
  })

  it('should have Gas Engine as the engine type in a meta div', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.meta').textContent).toContain('Gas Engine');
  })
});
