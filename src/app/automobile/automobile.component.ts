import { Component, OnInit, Input } from '@angular/core';
import { NgIf } from '@angular/common';

import { AutomobileForm } from '../automobile';
import { AutomobileManagerService } from '../automobile-manager.service';
import { LogMilesFormComponent } from '../log-miles-form/log-miles-form.component';

@Component({
  selector: 'app-automobile',
  templateUrl: './automobile.component.html',
  styleUrls: ['./automobile.component.css']
})
export class AutomobileComponent implements OnInit {
  @Input()automobile: AutomobileForm;
  showLogMiles: boolean;
  showChangeMaintenance: boolean;

  constructor(private automobileManagerService: AutomobileManagerService) { }

  ngOnInit() {
    this.showLogMiles = false;
    this.showChangeMaintenance = false;
   }

  maintainEngine(): void {
    this.automobileManagerService.maintainEngine(this.automobile.id);
  }

  rotateTires(): void {
    this.automobileManagerService.rotateTires(this.automobile.id);
  }

  replaceTires(): void {
    this.automobileManagerService.replaceTires(this.automobile.id);
  }

  replaceBrakes(): void {
    this.automobileManagerService.replaceBrakes(this.automobile.id);
  }

  onMilesLogged(event: number) {
    this.automobileManagerService.logMiles(this.automobile.id, event);
  }


  showMilesModal() {
    this.showLogMiles = !this.showLogMiles;
  }

  showMaintenanceModal() {
    this.showChangeMaintenance = !this.showChangeMaintenance;
  }

  onMilesClose(close: boolean) {
    this.showLogMiles = close;
  }

  onMaintenanceClose(close: boolean) {
    this.showChangeMaintenance = close;
  }

}
